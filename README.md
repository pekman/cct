# Descrição

CCT - **C**ontador de **C**iclos de **T**rabalho ao estilo pomodoro.

O CCT é um shell script simples, minimalista e leve para uso em um emulador de terminal;
 
 * Compatível com os padrões *Posix*,
 * Alinhado com a filosofia *Kiss*,
 * Alinhado com a filosofia *Unix*.
 
## Dependências

 * GNU Bash
 * libnotify
 * Programa de notificação: `dunst`,  `xfce4-notifyd` ou outro de sua preferência.

## Instalando o cct

usando o [GNU Wget](https://www.gnu.org/software/wget/):

~~~
wget https://codeberg.org/pekman/cct/raw/branch/master/cct -O /home/$USER/.local/bin/cct; chmod +x /home/$USER/.local/bin/cct
~~~

usando o [cURL](https://curl.haxx.se/):

~~~
curl -L https://codeberg.org/pekman/cct/raw/branch/master/cct -o /home/$USER/.local/bin/cct; chmod +x /home/$USER/.local/bin/cct
~~~

## Usando o cct

Para usar o CCT, execute a seguinte combinação do comando em um emulador de terminal:

~~~
cct nº_de_ciclos
~~~

Por exemplo:

~~~
cct 5
~~~

Executa 5 ciclos de trabalhos 